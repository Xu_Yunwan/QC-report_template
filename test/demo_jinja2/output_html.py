#!/usr/bin/env python
import jinja2
base_fn='template.html'
out_fn='test.html'
tmp_dir='.'
env = jinja2.Environment(loader=jinja2.FileSystemLoader(tmp_dir))
j_template = env.get_template(base_fn)
project_name='test'
samples=[
{'id':1},
{'id':2}
]
output=j_template.render(project_name=project_name,samples=samples)
f=open(out_fn,'w')
f.write(output)
f.close()

