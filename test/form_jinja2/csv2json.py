#!/usr/bin/env python
import sys
header=["sample_ID", 
        "description" , 
        "raw_reads_number" ,     
        "raw_Q20" , 
        "raw_Q30" ,  
        "raw_GC" ,   
        "clean_reads_number" ,   
        "clean_Q20" ,    
        "clean_Q30" ,    
        "clean_GC" ,     
        "clean_raw_ratio" ,  
        "mean_insert_size" ,     
        "coverage" ,     
        "aligned" ,  
        "dup" ]
print '''
"projects": [
'''
with open(sys.argv[1]) as f:
    project={'name':'','samples':[]}
    for line in f:
        tmp=line.strip().split(',')
        #print tmp
        if 'Project' in tmp[0]:
            #print tmp[0]
            print project,','
            project={'name':'','samples':[]}
            project['name']=tmp[0].split(' ')[1]
            project['samples']=[]
        if tmp[0]!='' and 'Project' not in tmp[0] and 'Sample_ID' not in tmp[0]:
            #print tmp[0]
            project['samples']+=[{
                    "sample_ID":"", 
                    "description":"", 
                    "raw_reads_number":"",   
                    "raw_Q20":"", 
                    "raw_Q30":"",    
                    "raw_GC":"",     
                    "clean_reads_number":"",     
                    "clean_Q20":"",  
                    "clean_Q30":"",  
                    "clean_GC":"",   
                    "clean_raw_ratio":"",    
                    "mean_insert_size":"",   
                    "coverage":"",   
                    "aligned":"",    
                    "dup":""
            }]
            for i,item in enumerate(tmp):
                project['samples'][-1][header[i]]=item
            
print project
print ']'
