#!/usr/bin/env python
import jinja2
import json
json_file='test.json'
base_fn='template.html'
out_fn='test.html'
tmp_dir='.'

projects=json.loads(open(json_file).read())
projects=projects["projects"]
env = jinja2.Environment(loader=jinja2.FileSystemLoader(tmp_dir))
j_template = env.get_template(base_fn)

output=j_template.render(projects=projects)
f=open(out_fn,'w')
f.write(output)
f.close()

