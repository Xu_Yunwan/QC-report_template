#!/usr/bin/env python

# -*- coding: utf-8 -*-

import jinja2
import json
import click

@click.command()
@click.option('-d','--data',required=True,help='<data.json>')
@click.option('-t','--template',default='template.html',required=True,help='template.html')
@click.option('-o','--out-put-html',required=True,help='<output html>')

def main(**options):
    json_file=options['data']
    base_fn=options['template']
    out_fn=options['out_put_html']
    tmp_dir='.'

    projects=json.loads(open(json_file).read())
    projects_general_stats=projects["projects_general_stats"]
    projects_QC=projects["projects_QC"]
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(tmp_dir))
    j_template = env.get_template(base_fn)

    output=j_template.render(projects_general_stats=projects_general_stats,projects_QC=projects_QC)
    f=open(out_fn,'w')
    f.write(output)
    f.close()

if __name__=='__main__':
    main()
